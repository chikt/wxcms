<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/12/13
  Time: 下午9:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <%--<link rel="stylesheet" href="//res.layui.com/layui/dist/css/layui.css"  media="all">--%>
    <link rel="stylesheet" href="${basePath}/static/plugins/layui/css/layui.css"  media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>

<%--查询条件以及操作按钮--%>
<br>
<div class="layui-form">
    <%--查询条件--%>
    <div class="layui-form-item layui-form-pane">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-inline">
            <input type="text" name="user_name" required  lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input" id="user_name">
        </div>
        <label class="layui-form-label">登录名</label>
        <div class="layui-input-inline">
            <input type="text" name="user_loginname" required layui-verity="required" placeholder="请输入登录名" autocomplete="off" class="layui-input" id="user_loginname">
        </div>
        <label class="layui-form-label">电话</label>
        <div class="layui-input-inline">
            <input type="text" name="user_phone" required  lay-verify="required" placeholder="请输入电话" autocomplete="off" class="layui-input" id="user_phone">
        </div>
    </div>
    <%--操作按钮--%>
    <div class="layui-form-item layui-form-pane">
        <label class="layui-form-label">邮箱</label>
        <div class="layui-input-inline">
            <input type="text" name="user_email" required  lay-verify="required" placeholder="请输入邮箱" autocomplete="off" class="layui-input" id="user_email">
        </div>
            <button class="layui-btn"  id="search">
                <i class="layui-icon">&#xe615;</i>搜索
            </button>
<shiro:hasPermission name="user:add">
            <button class="layui-btn" id="add">
                <i class="layui-icon">&#xe608;</i>添加
            </button>
</shiro:hasPermission>
            <%--<button class="layui-btn" id="deleteAll">--%>
                <%--<i class="layui-icon">&#xe640;</i>批量删除--%>
            <%--</button>--%>
    </div>
</div>


<%--分页数据列表--%>
<div class="layui-form">
    <table id="tablelist" class="layui-table" lay-data="{height: 'full-135', cellMinWidth: 80, page: true, limit:30, url:'${basePath}/user/pages'}" lay-filter="table">
        <thead>
        <tr>
            <th lay-data="{type:'checkbox'}">ID</th>
            <th lay-data="{field:'user_pk', width:100, sort: true}">ID</th>
            <th lay-data="{field:'user_loginname', width:100, sort: true}">登录名</th>
            <th lay-data="{field:'user_name', width:100}">姓名</th>
            <th lay-data="{field:'user_sex', width:100, sort: true,templet:'#switchTpl'}">性别</th>
            <th lay-data="{field:'user_idcard', sort: true, minWidth: 60, align: 'center'}">身份证号</th>
            <th lay-data="{field:'user_phone', minWidth: 150}">电话</th>
            <th lay-data="{field:'user_birthday', sort: true, align: 'center'}">生日</th>
            <th lay-data="{field:'user_email', sort: true, minWidth: 100, align: 'center'}">邮箱</th>
            <th lay-data="{field:'user_intime', sort: true, minWidth: 100, align: 'center'}">入职日期</th>

            <th lay-data="{fixed: 'right', width:178, align:'center', toolbar: '#barDemo'}"></th>
        </tr>
        </thead>
    </table>
</div>

<script type="text/html" id="barDemo">
<shiro:hasPermission name="user:edit">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
</shiro:hasPermission>
<shiro:hasPermission name="user:delete">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</shiro:hasPermission>
</script>


<script src="${basePath}/static/plugins/layui/layui.js"></script>
<script type="text/html" id="switchTpl">
    <!-- 这里的 checked 的状态只是演示 -->
    {{#
    if(d.user_sex === '0'){
    }}
    <span style="color: #1110f5;">女</span>
    {{#
    }
    }}
    {{#
    if(d.user_sex === '1'){
    }}
    <span style="color: #f5120f;">男</span>
    {{#
    }
    }}
</script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use('table', function(){
        var $ = layui.jquery;
        var table = layui.table;

        //监听表格复选框选择
        table.on('checkbox(table)', function(obj){
            console.log(obj)
        });

        //监听工具条
        table.on('tool(table)', function(obj){
            var data = obj.data;
            if(obj.event === 'del'){
                user_delete(data.user_pk);
            } else if(obj.event === 'edit'){
//                layer.alert('编辑行：<br>'+ JSON.stringify(data))
                edit(data.user_pk);
            }
        });


        $('#add').on('click', function() {
            layer.open({
                title: '添加',
                maxmin: true,
                type: 2,
                content: 'edit.jsp',
                area: ['500px', '520px'],
                success: function(layero, index) {

                },
                end : function () {
                    table.reload("tablelist");
                }
            });
        });


        //过滤查询
        $("#search").on('click',function () {
            //执行重载
            table.reload('tablelist', {
                url: '${basePath}/user/pages',

                where: {
//                    key: {
                        user_name:$("#user_name").val(),
                        user_loginname:$("#user_loginname").val(),
                        user_phone:$("#user_phone").val(),
                        user_email:$("#user_email").val()
//                    }
                  },
                  page: {
                      curr: 1 //重新从第 1 页开始
                  }

            });
          
            
        });


        //编辑操作
        edit = function (data){
            layer.open({
                title : '编辑',
                maxmin : true,
                type : 2,
                content : 'edit.jsp',
                area : ['500px','520px'],
                success : function(layero,index){
                    var body = layer.getChildFrame('body', index); //巧妙的地方在这里哦
                    body.contents().find("#user_pk").val(data);
                },
                end : function () {
                    table.reload("tablelist");
                }
            });
        }


        //删除记录
        user_delete = function (data) {
            var flag = false;
            layer.confirm("确认删除此数据吗？", {icon: 3, title: '提示'},
                function (index) {      //确认后执行的操作
                    $.ajax({
                        type: 'post',
                        url: '${basePath}/user/delete',
                        data: {user_pk: data},
                        success: function (response) {
                            if (response.state == "success") {
                                layer.close(index);
                                parent.layer.msg("删除成功");
                                table.reload("tablelist");
                            } else if (response.state == "fail") {
                                parent.layer.alert(response.msg);
                            }
                        },
                        error: function (response) {
                            layer.close(index);
                            parent.layer.alert(response.msg);
                        }
                    });
                },
                function (index) {      //取消后执行的操作
                    flag = false;
                });
        }

    });
</script>


</body>
</html>
