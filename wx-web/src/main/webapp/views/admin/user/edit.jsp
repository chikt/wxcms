<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/6/20
  Time: 上午10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">


    <title>后台管理系统</title>
    <% String path = request.getContextPath(); %>
    <link rel="stylesheet" href="${basePath}/static/plugins/layui/css/layui.css" media="all">

</head>
<body>
<div class="layui-form" >
<div class="container">

    <form id="userForm" method="post" class="layui-form" action="">
           <input id="user_pk" name="user_pk" type="hidden" />

           <div class="layui-form-item" style="margin: 5% auto">
               <label class="layui-form-label">用户名</label>
               <div class="layui-input-block">
                   <input id="user_name" name="user_name"  lay-verify="required"  autocomplete="off" placeholder="请输入姓名" class="layui-input" type="text" />
               </div>
           </div>
           <div class="layui-form-item">
               <label class="layui-form-label">性别</label>
               <div class="layui-input-block">
                   <input type="radio" id="user_female" name="user_sex" value="1" title="男" checked>
                   <input type="radio" id="user_sex" name="user_sex" value="0" title="女"   >
               </div>
           </div>
            <div class="layui-form-item">
                <label class="layui-form-label">登录名</label>
                <div class="layui-input-block">
                    <input id="user_idcard" name="user_idcard" autocomplete="off" placeholder="请输入身份证号" class="layui-input" type="text">
                </div>
            </div>
           <div class="layui-form-item">
               <label class="layui-form-label">登录名</label>
               <div class="layui-input-block">
                   <input id="user_loginname" name="user_loginname" lay-verify="required" autocomplete="off" placeholder="请输入登录名" class="layui-input" type="text">
               </div>
           </div>
           <div class="layui-form-item">
               <label class="layui-form-label">密码</label>
               <div class="layui-input-block">
                   <input id="user_password" name="user_password" lay-verify="required"  autocomplete="off" placeholder="请输入密码" class="layui-input" type="password" >
               </div>
           </div>
           <div class="layui-form-item">
               <label class="layui-form-label">确认密码</label>
               <div class="layui-input-block">
                   <input id="user_repassword" name="user_repassword"  lay-verify="user_repassword"  autocomplete="off" placeholder="请输入确认密码" class="layui-input" type="password">
               </div>
           </div>


           <div class="layui-form-item">
               <label class="layui-form-label">手机号码</label>
               <div class="layui-input-block">
                   <input id="user_phone" name="user_phone" autocomplete="off" placeholder="请输入手机号码" class="layui-input" type="text" />
               </div>
           </div>

           <div class="layui-form-item">
               <label class="layui-form-label">电子邮箱</label>
               <div class="layui-input-block">
                   <input id="user_email" name="user_email" autocomplete="off" placeholder="请输入电子邮箱" class="layui-input" type="text" />
               </div>
           </div>

            <div class="layui-form-item">
                <label class="layui-form-label">出生日期</label>
                <dvi class="layui-input-inline">
                    <input id="user_birthday" name="user_birthday" class="layui-input" type="text" placeholder="自定义日期格式">
                </dvi>
            </div>

           <div class="layui-form-item">
               <label class="layui-form-label">录入时间</label>
               <dvi class="layui-input-inline">
                   <input id="user_intime" name="user_intime" class="layui-input" type="text" placeholder="自定义日期格式">
               </dvi>
           </div>


           <div class="layui-form-item">
               <div align="center" class="layui-input-block" style="margin: 5% auto">
                   <button class="layui-btn layui-btn-small" align="center" id="edit">保存</button>
                   <button class="layui-btn layui-btn-small" id="reset" align="center" type="reset">重置</button>
               </div>
           </div>

    </form>
</div>
</div>
</body>


<script src="<%=path%>/static/plugins/layui/layui.js"></script>
<%--layui--%>
<script>
    layui.use(['form','laydate'],function () {
        var $ = layui.jquery;
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;

        //执行一个laydate实例
        laydate.render({
            elem: '#user_birthday' //指定元素
        });
        laydate.render({
            elem: '#user_intime' //指定元素
        });

        $(function (){
            var user_pk=$("#user_pk").val();
            if(user_pk!=null && user_pk!=''){
               $.ajax({
                   type:'post',
                   url:"<%=path%>/user/list",
                   data:{user_pk:user_pk},
                   success:function (response) {
                       var json = eval(response); // 数组
                       $.each(json,function (i) {
                           $("#user_pk").val(json[i].user_pk);
                           $("#user_name").val(json[i].user_name);
//                           $("#user_sex").val(json[i].user_sex);
                           $("#user_loginname").val(json[i].user_loginname);
                           $("#user_password").val(json[i].user_password);
                           $("#user_repassword").val(json[i].user_password);
                           $("#user_phone").val(json[i].user_phone);
                           $("#user_email").val(json[i].user_email);
                           $("#user_birthday").val(json[i].user_birthday);
                           $("#user_intime").val(json[i].user_intime);
                           $("#user_idcard").val(json[i].user_idcard);
                           $(":radio[name='user_sex'][value='" + json[i].user_sex + "']").prop("checked", "checked");
                       });
                       form.render('select');
                       form.render('radio');
                   }
               });
            }
        });


        $("#edit").on('click',function(){
            var url="";
            var user_pk=$("#user_pk").val();
            if(user_pk!=null && user_pk!=''){
                url="<%=path%>/user/update";
            }else{
                url="<%=path%>/user/save";
            }
            form.verify({
                user_repassword : function (value) {
                    var user_password=$("#user_password").val();
                    if(user_password != value){
                        return '两次输入的密码不一致';
                    }
                    $.ajax({
                        type : 'post',
                        async : false,//同步请求，执行成功后才会继续执行后面的代码
                        url : url,
                        data:{
                            user_pk:user_pk,
                            user_name:$("#user_name").val(),
                            user_loginname:$("#user_loginname").val(),
                            user_password:$("#user_password").val(),
                            user_phone:$("#user_phone").val(),
                            user_email:$("#user_email").val(),
                            user_intime:$("#user_intime").val(),
                            user_birthday:$("#user_birthday").val(),
                            user_idcard:$("#user_idcard").val(),
                            user_sex:$("input[name='user_sex']:checked").val(),
                        },
                        success:function(response){
                            parent.layer.alert(response.msg);
                            window.location.reload();
                        },
                        error:function (response) {
                            parent.layer.alert(response.msg);
                            window.location.reload();
                        }
                    });
                    return false;
                }
            });

        });

    })

</script>
</html>
