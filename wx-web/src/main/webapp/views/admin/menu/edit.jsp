<!DOCTYPE html>
<%--
  Created by IntelliJ IDEA.
  User: myd
  Date: 2017/6/20
  Time: 上午10:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <% String path = request.getContextPath(); %>
    <title>后台管理系统</title>
    <link rel="stylesheet" href="<%=path%>/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="<%=path%>/static/css/font-awesome/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="css/admin.css"/> -->
</head>
<body>
<div id="app" class="layui-form">
    <div class="container">
        <form class="layui-form" action="" id="formlist">
            <input id="menu_pk" name="menu_pk"  autocomplete="off"  class="layui-input" type="hidden"/>
            <div class="layui-form-item" style="margin: 5% auto">
                <label class="layui-form-label">菜单名称</label>
                <div class="layui-input-block">
                    <input id="menu_name" name="menu_name"  lay-verify="required"  autocomplete="off" placeholder="请输入菜单名称" class="layui-input" type="text"/>
                </div>
            </div>
            <div class="layui-form-item">
                <%--<label class="layui-form-label">菜单图标</label>--%>
                <%--<div class="layui-input-block">--%>
                    <%--<input id="menu_icon" name="menu_icon"  autocomplete="off" placeholder="请输入菜单图标" class="layui-input" type="text">--%>
                <%--</div>--%>
                    <div class="layui-inline">
                        <label class="layui-form-label">菜单图标</label>
                        <div class="layui-form-mid layui-word-aux">
                            <%--<a class="layui-btn layui-btn-mini"><i id="menu_icon1" class="layui-icon"></i></a>--%>
                            <input class="layui-input" id="menu_icon" name="menu_icon" value="" type="hidden"/>
                            <div  id="menu_icon1" class="layui-input-inline">
                                <li class="layui-icon" style="font-size: 30px;">&#xe641;</li>
                            </div>
                        </div>
                        <div class="layui-form-mid layui-word-aux">
                            <a class="layui-btn layui-btn-mini select_img" data-id="" title="选择图标"><i class="layui-icon">&#xe64a;</i></a>
                        </div>
                    </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">父菜单</label>
                <div class="layui-input-block">
                    <select id="menu_parent" name="menu_parent"  lay-verify="required">
                        <option value=""></option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">菜单url</label>
                <div class="layui-input-block">
                    <input id="menu_url" name="menu_url"  autocomplete="off" placeholder="请输入菜单url" class="layui-input" type="text">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">序号</label>
                <div class="layui-input-block">
                    <input id="menu_number" name="menu_number" autocomplete="off" placeholder="请输入排序值" class="layui-input" type="text"/>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">类型</label>
                <div class="layui-input-block">
                    <input type="radio" id="menu_type"  name="menu_type" value="0" title="菜单" checked >
                    <input type="radio"  id="menu_type1" name="menu_type" value="1" title="按钮" >
                </div>
            </div>


            <div class="layui-form-item">
                <label class="layui-form-label">状态</label>
                <div class="layui-input-block">
                    <input type="radio" id="menu_status" name="menu_state" value="0" title="启用" checked >
                    <input type="radio" id="menu_status1" name="menu_state" value="1" title="禁用" >
                </div>
            </div>


            <div class="layui-form-item">
                <div align="center" class="layui-input-block" style="margin: 5% auto">
                    <button class="layui-btn" align="center" onclick="save()">保存</button>
                    <button class="layui-btn" id="reset" align="center" type="reset">重置</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="<%=path%>/static/plugins/layui/layui.js"></script>
</body>
<%--layui--%>
<script>
    layui.use(['form','jquery'],function () {
        var $=layui.jquery;
        var form=layui.form;

        //选择图标
        $(".select_img").click(function(){
            var index = top.layui.layer.open({
                title : '<i class="larry-icon larry-tupianguanli"></i>选择图标',
                type : 2,
                skin : 'layui-layer-molv',
                content : '<%=path%>/views/admin/menu/img.jsp',
                area: ['485px', '370px '],
                resize:false,
                anim:1,
                success : function(layero, index){


                }
            })
        })

        edit=function(){
            var edit_url = "<%=path%>/menu/list";
            var menu_pk = $('#menu_pk').val();
            debugger;
             if (menu_pk=="")
             {
                 return false;
             }
            $.ajax({
                url:edit_url,
                type:"post",
                data:{menu_pk:menu_pk},//form id
                datatype:"json",
                success:function(response){
                    var data = eval(response);
                    $('#menu_pk').val(data[0].menu_pk);
                    $('#menu_name').val(data[0].menu_name);
                    $('#menu_icon').val(data[0].menu_icon);
                    $('#menu_icon1').html("<li class='layui-icon' style='font-size: 30px;'>"+data[0].menu_icon+"</li>")
                    $('#menu_number').val(data[0].menu_number);
                    $('#menu_parent').val(data[0].menu_parent);
                    $('#menu_url').val(data[0].menu_url);
                    $(":radio[name='menu_state'][value='" + data[0].menu_state + "']").prop("checked", "checked");
                    $(":radio[name='menu_type'][value='" + data[0].menu_type + "']").prop("checked", "checked");
                    form.render('select');
                    form.render('radio');

                },
                error:function (response) {
                    parent.layer.alert(response.msg);
                }

            });
            return false;
        };

        $(function () {
           edit()
        });

        /**
         * 给下拉菜单绑定值
         * @type {string}
         */
        var url = "<%=path%>/menu/list?menu_type=0";
        $.getJSON(url, function(response) {
            $("#menu_parent").append("<option value='0' selected=''>无</option>");  //添加一项option
            console.log(response);
            var json = eval(response); // 数组
            $.each(json, function(index, item) {
                var menu_pk = json[index].menu_pk;
                var menu_name = json[index].menu_name;
                $("#menu_parent").append("<option value=" + menu_pk + ">" + menu_name + "</option>"); // 添加一项option
            });
            form.render('select');
        });

        /**
         * 保存信息
         */
        save = function () {
            $.ajax({
                url:"<%=path%>/menu/save",
                type:"post",
                async : false,//同步请求，执行成功后才会继续执行后面的代码
                data:$('#formlist').serialize(),//form id
                datatype:"json",
                success:function(response){
                    debugger;
                    parent.layer.alert(response.msg);
                },
                error:function (response) {
                    parent.layer.alert(response.msg);
                }

            });
            return false;
        }

    })
</script>
</html>
