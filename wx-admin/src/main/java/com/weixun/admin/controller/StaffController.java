package com.weixun.admin.controller;


import com.jfinal.json.JFinalJson;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.comm.controller.BaseController;
import com.weixun.model.SysStaff;
import com.weixun.admin.service.StaffService;
import com.weixun.utils.ajax.AjaxMsg;
import com.weixun.utils.md5.Md5Utils;

import java.util.List;

/**
 * Created by myd on 2017/4/14.
 */

public class StaffController extends BaseController {

    StaffService staffService = new StaffService();

    /**
     * 保存方法
     */
    public void save()
    {
//        new SysStaff().set()
//        getModel()
        //前台页面的model名称，没有则为空，否则映射不到值
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;

        try {

            SysStaff staff = getModel(SysStaff.class,"");
            String tmp = Md5Utils.ToMd5(staff.getStaffPassword());
            String passwd1 = Md5Utils.ToMd5(staff.getStaffPassword(),2);
            String passwd = Md5Utils.ToMd5(staff.getStaffPassword(),1);
            staff.setStaffPassword(Md5Utils.ToMd5(passwd,1));

            if (staff.getStaffPk() != null && !staff.getStaffPk().equals(""))
            {
                //更新方法
                res = staff.update();
                if(res)
                {
                    ajaxMsg.setState("success");
                    ajaxMsg.setMsg("修改成功");
                }
                else
                {
                    ajaxMsg.setState("fail");
                    ajaxMsg.setMsg("修改失败");
                }
            }
            else {
                //保存方法
                res = staff.save();
                if(res)
                {
                    ajaxMsg.setState("success");
                    ajaxMsg.setMsg("保存成功");
                }
                else
                {
                    ajaxMsg.setState("fail");
                    ajaxMsg.setMsg("保存失败");
                }
            }
//        getBean(Staff.class).save();
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("保存失败");
        }
        renderJson(ajaxMsg);

    }

    /**
     * 更新数据
     */
    public void update()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        boolean res = false;

        try {

            SysStaff staff = getModel(SysStaff.class,"");
            staff.setStaffPassword(Md5Utils.ToMd5(staff.getStaffPassword(),1));
            //更新方法
            res = staff.update();
            if(res)
            {
                ajaxMsg.setState("success");
                ajaxMsg.setMsg("保存成功");
            }
            else
            {
                ajaxMsg.setState("fail");
                ajaxMsg.setMsg("保存失败");
            }
//        getBean(Staff.class).save();
        }catch (Exception e)
        {
            e.getMessage();
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("保存失败");
        }
        renderJson(ajaxMsg);
    }


    /**
     * 删除数据
     */
    public void delete()
    {
        AjaxMsg ajaxMsg = new AjaxMsg();
        String staff_pk = this.getPara("staff_pk");
        int res =staffService.deleteById(staff_pk);
        if (res >0) {
            ajaxMsg.setState("success");
            ajaxMsg.setMsg("删除成功");
        }
        else
        {
            ajaxMsg.setState("fail");
            ajaxMsg.setMsg("删除失败");
        }

        renderJson(ajaxMsg);
    }


    /**
     * 查询数据列表
     */
    public void list()
    {
        String staff_pk = getPara("staff_pk");
        List<Record> records = staffService.findList(staff_pk);
        renderJson(JFinalJson.getJson().toJson(records));
    }


    /**
     * 基于layui的分页
     */
    public void pages(){
        try {


            int pageNumber = getParaToInt("page");
            int pageSize = getParaToInt("limit");
            String staff_name = getPara("staff_name");
            String staff_loginname = getPara("staff_loginname");
            String staff_phone = getPara("staff_phone");
            String staff_email = getPara("staff_email");
            Page<Record> userList = staffService.paginate(pageNumber, pageSize, staff_name,staff_loginname,staff_phone,staff_email);//获得用户信息
            renderPageForLayUI(userList);
        }catch (Exception e)
        {
            e.getMessage();
        }
    }

}
